<?php
/**
 * @author Rizart Dokollari <r.dokollari@gmail.com>
 * @since 12/15/17
 */

namespace App;

class ClassifyNumber
{
    /** @var AliquotSum */
    private $aliquotSum;

    public function __construct(AliquotSum $aliquotSum)
    {
        $this->aliquotSum = $aliquotSum;
    }

    public function handle($number)
    {
        $aliquotSum = $this->aliquotSum->handle($number);

        if ($aliquotSum > $number) return 'abundant';

        if ($aliquotSum < $number) return 'deficient';

        return 'perfect';
    }
}