<?php
/**
 * @author Rizart Dokollari <r.dokollari@gmail.com>
 * @since 12/15/17
 */

namespace App;

class AliquotSum
{
    public function handle($number)
    {
        $this->guard($number);

        if($number == 1) return 0;

        $sum = 1;

        $maxNumber = (int)sqrt($number);

        /** @credits https://softwareengineering.stackexchange.com/a/264072/112278 */
        for ($index = 2; $index <= $maxNumber; $index++) {

            if ($number % $index) {
                continue;
            }

            $sum += $index;

            $correspondingDivisor = $number / $index;

            if ($correspondingDivisor != $index) {
                $sum += $correspondingDivisor;
            }
        }

        return $sum;
    }

    private function guard($number)
    {
        if (empty($number) || $number < 1 || !is_integer($number)) {
            throw new \InvalidArgumentException('Number is invalid.');
        }
    }
}