<?php
/**
 * @author Rizart Dokollari <r.dokollari@gmail.com>
 * @since 12/15/17
 */

function getClassification($input)
{
    $classifyNumber = new \App\ClassifyNumber(
        new \App\AliquotSum()
    );

    return $classifyNumber->handle($input);
}

function dd($variable)
{
    var_dump($variable);
    exit;
}
