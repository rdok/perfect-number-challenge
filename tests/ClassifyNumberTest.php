<?php
/**
 * @author Rizart Dokollari <r.dokollari@gmail.com>
 * @since 12/15/17
 */

namespace Tests;


use App\AliquotSum;
use App\ClassifyNumber;
use InvalidArgumentException;

class ClassifyNumberTest extends TestCase
{
    /** @var  ClassifyNumber */
    protected $classifyNumber;

    public function setUp()
    {
        parent::setUp();

        $this->classifyNumber = new ClassifyNumber(new AliquotSum);
    }

    /**
     * @test
     * @dataProvider  perfectNumbers
     */
    public function classify_perfect_numbers($perfectNumber)
    {
        $actual = $this->classifyNumber->handle($perfectNumber);

        $this->assertEquals('perfect', $actual);
    }

    /** @see https://en.wikipedia.org/wiki/List_of_perfect_numbers */
    public function perfectNumbers()
    {
        return [[6], [28], [496], [8128]];
    }

    /**
     * @test
     * @dataProvider  abundantNumbers
     */
    public function classify_abundant_numbers($perfectNumber)
    {
        $actual = $this->classifyNumber->handle($perfectNumber);

        $this->assertEquals('abundant', $actual);
    }

    /** @see https://en.wikipedia.org/wiki/Abundant_number#Examples */
    public function abundantNumbers()
    {
        return [[12], [18], [20], [30], [36]];
    }

    /**
     * @test
     * @dataProvider  deficientNumbers
     */
    public function classify_deficient_numbers($deficientNumber)
    {
        $actual = $this->classifyNumber->handle($deficientNumber);

        $expected = 'deficient';

        $message = sprintf(
            'Failed asserting that %s is classified as %s.',
            $deficientNumber, $actual
        );

        $this->assertEquals($expected, $actual, $message);
    }

    /** @see https://en.wikipedia.org/wiki/Deficient_number#Examples */
    public function deficientNumbers()
    {
        return [
            [1], [2], [3], [4], [5], [7], [8], [9], [10], [11], [13], [14],
            [15], [16], [17], [19], [21], [22], [23], [25]
        ];
    }
}