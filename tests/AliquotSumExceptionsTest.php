<?php
/**
 * @author Rizart Dokollari <r.dokollari@gmail.com>
 * @since 12/15/17
 */

namespace Tests;

use App\AliquotSum;
use App\ClassifyNumber;
use InvalidArgumentException;

class AliquotSumExceptionsTest extends TestCase
{
    const NUMBER_IS_INVALID = 'Number is invalid';

    /** @var  AliquotSum */
    protected $aliquotSum;

    public function setUp()
    {
        parent::setUp();

        $this->aliquotSum = new AliquotSum;
    }

    /** @test */
    public function throw_exception_when_number_is_empty()
    {
        $this->expectException(InvalidArgumentException::class);

        $this->expectExceptionMessage(self::NUMBER_IS_INVALID);

        $this->aliquotSum->handle(null);
    }

    /** @test */
    public function throw_exception_when_number_is_negative()
    {
        $this->expectException(InvalidArgumentException::class);

        $this->expectExceptionMessage(self::NUMBER_IS_INVALID);

        $this->aliquotSum->handle(-1);
    }

    /** @test */
    public function throw_exception_when_number_is_not_integer()
    {
        $this->expectException(InvalidArgumentException::class);

        $this->expectExceptionMessage(self::NUMBER_IS_INVALID);

        $this->aliquotSum->handle(5.5);
    }

    /** @test */
    public function throw_exception_when_number_is_not_numeric()
    {
        $this->expectException(InvalidArgumentException::class);

        $this->expectExceptionMessage(self::NUMBER_IS_INVALID);

        $this->aliquotSum->handle('invalid');
    }
}