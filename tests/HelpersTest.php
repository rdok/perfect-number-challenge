<?php
/**
 * @author Rizart Dokollari <r.dokollari@gmail.com>
 * @since 12/15/17
 */

namespace Tests;

class HelpersTest extends TestCase
{
    /** @test */
    public function use_classify_number_object_as_a_global_function()
    {
        $this->assertEquals('perfect', getClassification(6));

        $this->assertEquals('abundant', getClassification(36));

        $this->assertEquals('deficient', getClassification(25));
    }
}