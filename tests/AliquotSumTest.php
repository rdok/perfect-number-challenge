<?php
/**
 * @author Rizart Dokollari <r.dokollari@gmail.com>
 * @since 12/15/17
 */

namespace Tests;

use App\AliquotSum;

class AliquotSumTest extends TestCase
{
    /** @var  AliquotSum */
    protected $aliquotSum;

    public function setup()
    {
        $this->aliquotSum = new AliquotSum;
    }

    /**
     * @test
     * @dataProvider  aliquotSums
     */
    public function calculate_aliquot_sum($number, $expectedAliquotSum)
    {
        $actualAliquotSum = $this->aliquotSum->handle($number);

        $message = sprintf(
            'Failed asserting that %s matches expected %s for number %s.',
            $actualAliquotSum, $expectedAliquotSum, $number
        );

        $this->assertEquals($expectedAliquotSum, $actualAliquotSum, $message);
    }

    /** @see https://en.wikipedia.org/wiki/Table_of_divisors */
    public function aliquotSums()
    {
        return [[1, 0], [2, 1], [3, 1], [4, 3], [15, 9], [21, 11], [42, 54]];
    }

    /** @test */
    public function calculate_aliquot_sum_for_large_numbers()
    {
        $start = microtime(true);

        $this->aliquotSum->handle(999999999999999);

        $end = microtime(true);

        $executionTime = abs($end - $start);

        $this->assertLessThan(1, $executionTime);
    }
}