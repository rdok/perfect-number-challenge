# Perfect Number Challenge (PHP) #

> We would like you to write a function in PHP to determine whether a given number is [perfect](https://en.wikipedia.org/wiki/Perfect_number), [abundant](https://en.wikipedia.org/wiki/Perfect_number), or [deficient](https://en.wikipedia.org/wiki/Deficient_number). This is based on the classification scheme for natural numbers by [Nicomachus](https://en.wikipedia.org/wiki/Nicomachus).

You can find the requested function in `/src/helpers.php`.


### Installation
- `git clone git@bitbucket.org:rdok/perfect-number-challenge.git`
- `cd perfect-number-challenge`
- `composer install`

### Tests
- `./vendor/bin/phpunit`
